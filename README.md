# Summary

Implement something like the following in a REST way.

http://104.236.170.167/

# Specification

* The back-end server returns data provided as json files according to the request.
    * https://bitbucket.org/daiki_/joao-back/src/master/resources/

* The front-end sends requests to the backend server and draws a table based on the data returned by the back-end server.

* If you have time,
    * add sorting, filtering and/or pagination functionality.
    * put the data in json files to any database you like.

* Focus on the functionality rather than the UI.

# Regulation

* Use this repository for the front-end source code.
* Use the following repository for the back-end source code.
    * https://bitbucket.org/daiki_/joao-back/

* Once done or time's up,
    * Send a Pull Request with the completed source code to each repository.
    * Deploy the app to somewhere publicly available and send the URL to us. (email us or add the url here)

* Both of the above must be done within 3 hours.

* Use React.js for the front-end. Use any language you like for the back-end.

