import React from 'react';
import { Link } from 'react-router';


/**
 * <Main/> component
 *
 * @return {JSXElement}
 */
const Main = ():Object => {

  return (
    <div className="app">
      <Link to={'/foods'}>  Food  </Link>
      <Link to={'/drinks'}>  Drink  </Link>
    </div>
  );
}

export default Main;
