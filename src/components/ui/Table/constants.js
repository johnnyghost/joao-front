export const columns = [{
  title: 'No.',
  dataIndex: 'id',
  key:'id',
  width: 300
}, {
  title: 'Category',
  dataIndex: 'Category',
  key:'Category',
  width: 300
}, {
  title: 'Name',
  dataIndex: 'Name',
  key:'Name',
  width: 300
}, {
  title: 'Register Date',
  dataIndex: 'RegisterDate',
  key:'RegisterDate',
  width: 300,
  render: (data:any):string => {
    // console.log(data);
    const date = new Date(data);

    return (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
  }
}];
