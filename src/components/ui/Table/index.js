import React from 'react';
import RCTable from 'rc-table';
import { columns } from './constants';

type TableType = {
  data: Array
}
/**
 * Wrapper on RCTable.
 *
 * @see more info in https://github.com/react-component/table
 * @returns {JSXElement}
 */
const Table = ({data}:TableType):Object => {
  return (
    <RCTable
      data={data}
      columns={columns}
    />
  )
}

export default Table;
