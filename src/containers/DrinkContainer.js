import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { drinkActionCreators }  from 'store/actions/';

import { Table } from 'components/ui';
/**
 * <DrinkContainer />
 *
 * @return {JSXElement}
 */
class DrinkContainer extends Component {

  /**
   * Component is rendered.
   */
  componentDidMount() {
    this.props.drinkActions.fetchDrinks();
  }
  /**
   * Get the food list data.
   *
   * @method drinkList
   * @return {Array} The food list array
   */
  get drinkList():Array {
    return this.props.drinkList;
  }
  /**
   * Renders <DrinkContainer />
   *
   * @return {JSXElement}
   */
  render():Object {
    return (
      <div>
        <Table data={this.drinkList}/>
      </div>
    );
  }
}

/**
 * Will subscribe to Redux store updates
 *
 * @method
 *
 * @param  {Object} state Store’s state
 * @return {Object} and returns an object to be passed as props
 */
const mapStateToProps = (state:Object):Object => {
  return {
    drinkList: state.drinks.list
  }
};

/**
 * Bound to the store.
 * Will be merged into the component’s props.
 *
 * @method
 *
 * @param {Function} dispatch Dispatch function
 * @return {Object}
 */
const mapDispatchToProps = (dispatch:Function):Object => ({
  drinkActions : bindActionCreators(drinkActionCreators, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrinkContainer);
