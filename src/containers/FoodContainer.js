import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { foodActionCreators }  from 'store/actions/';

import { Table } from 'components/ui';
/**
 * <FoodContainer />
 *
 * @return {JSXElement}
 */
class FoodContainer extends Component {

  /**
   * Component is rendered.
   */
  componentDidMount() {
    this.props.foodActions.fetchFoods();
  }
  /**
   * Get the food list data.
   *
   * @method foodList
   * @return {Array} The food list array
   */
  get foodList():Array {
    return this.props.foodList;
  }
  /**
   * Renders <FoodContainer />
   *
   * @return {JSXElement}
   */
  render():Object {
    return (
      <div>
        <Table data={this.foodList}/>
      </div>
    );
  }
}

/**
 * Will subscribe to Redux store updates
 *
 * @method
 *
 * @param  {Object} state Store’s state
 * @return {Object} and returns an object to be passed as props
 */
const mapStateToProps = (state:Object):Object => {
  return {
    foodList: state.foods.list
  }
};

/**
 * Bound to the store.
 * Will be merged into the component’s props.
 *
 * @method
 *
 * @param {Function} dispatch Dispatch function
 * @return {Object}
 */
const mapDispatchToProps = (dispatch:Function):Object => ({
  foodActions : bindActionCreators(foodActionCreators, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FoodContainer);
