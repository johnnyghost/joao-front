const BASE_URL = 'https://nexgent-server.herokuapp.com';

/**
 * Github service.
 * Abstraction of the github API.
 */
const apiService = {

  /**
   * Returns a list of foods.
   *
   * @return {Promise<Array>} An array of food
   */
  getFoods ():Promise {
    return fetch(`${BASE_URL}/food`)
      .then(successResponseHandler)
      .then((data:Object):Object => {
        return data;
      })
  },

  /**
   * Returns a list of drinks.
   *
   * @return {Promise<Array>} An array of food
   */
  getDrinks ():Promise {
    return fetch(`${BASE_URL}/drink`)
      .then(successResponseHandler)
      .then((data:Object):Object => {
        return data;
      })
  }
}

/**
 * Success response handler.
 *
 * @param {Object} response The response Object
 * @return {Object}
 */
const successResponseHandler = ((response:Object):Object => response.json());

export default apiService;
