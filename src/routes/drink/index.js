import { DrinkContainer } from 'containers';

module.exports = {
  path: 'drinks',
  getComponents(location:?Object, cb:Function) {
    cb(null, DrinkContainer);
  }
};
