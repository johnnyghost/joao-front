import { FoodContainer } from 'containers';

module.exports = {
  path: 'foods',
  getComponents(location:?Object, cb:Function) {
    cb(null, FoodContainer);
  }
};
