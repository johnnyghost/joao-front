import DRINK_CONSTANTS from 'store/constants/drink';
import { ActionType } from 'store/types/ActionType';

/**
 * Dispatch an action to get the list of drinks.
 *
 * @method custom
 * @public
 *
 * @return {Object}
 */
export const fetchDrinks = ():ActionType => (
  {
    type: DRINK_CONSTANTS.FETCH_DRINK
  }
)

/**
 * Dispatch an action to get the response of all drinks
 *
 * @method custom
 * @public
 *
 * @param {Array} payload The array of food
 *
 * @return {Object}
 */
export const getDrinksSuccess = (payload:Array<Object>):ActionType => (
  {
    type: DRINK_CONSTANTS.GET_DRINK_SUCCESS,
    payload
  }
)
