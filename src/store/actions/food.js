import FOOD_CONSTANTS from 'store/constants/food';
import { ActionType } from 'store/types/ActionType';

/**
 * Dispatch an action to get the list of foods.
 *
 * @method custom
 * @public
 *
 * @return {Object}
 */
export const fetchFoods = ():ActionType => (
  {
    type: FOOD_CONSTANTS.FETCH_FOOD
  }
)

/**
 * Dispatch an action to get the response of all foods
 *
 * @method custom
 * @public
 *
 * @param {Array} payload The array of food
 *
 * @return {Object}
 */
export const getFoodsSuccess = (payload:Array<Object>):ActionType => (
  {
    type: FOOD_CONSTANTS.GET_FOOD_SUCCESS,
    payload
  }
)
