import * as foodActionCreators from './food';
import * as drinkActionCreators from './drink';

export {
  foodActionCreators,
  drinkActionCreators
};
