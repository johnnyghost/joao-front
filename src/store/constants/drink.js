import { createConstants } from 'utils/redux';

const apiConstants = createConstants(
  'FETCH_DRINK',
  'GET_DRINK_SUCCESS',
  'ERROR'
);

export default apiConstants;
