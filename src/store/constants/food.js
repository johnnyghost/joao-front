import { createConstants } from 'utils/redux';

const apiConstants = createConstants(
  'FETCH_FOOD',
  'GET_FOOD_SUCCESS'
);

export default apiConstants;
