import DRINK_CONSTANTS from 'store/constants/drink';
import { createReducer } from 'utils/redux';

/**
 * The initial state.
 * @type {Object}
 */
const initialState = {
  list: []
};

/**
 * Get the drinks
 *
 * @method get
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const getDrinks = (state:Object, data:Object):Object => {
  return {
    ...state,
    list: data
  };
}

export default createReducer(initialState, {
  [DRINK_CONSTANTS.GET_DRINK_SUCCESS]: getDrinks
});
