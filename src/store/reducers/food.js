import FOOD_CONSTANTS from 'store/constants/food';
import { createReducer } from 'utils/redux';

/**
 * The initial state.
 * @type {Object}
 */
const initialState = {
  list: []
};

/**
 * Get the foods
 *
 * @method getFoods
 *
 * @param  {Object} state The state
 * @param  {Object} data  The data received
 *
 * @return {Object} The current state
 */
const getFoods = (state:Object, data:Object):Object => {
  return {
    ...state,
    list: data
  };
}

export default createReducer(initialState, {
  [FOOD_CONSTANTS.GET_FOOD_SUCCESS]: getFoods
});
