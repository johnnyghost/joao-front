import { combineReducers } from 'redux';

// reducers
import foodReducer from './food';
import drinkReducer from './drink';

/**
 * Combine all reducers.
 */
const appReducer = combineReducers({
  foods: foodReducer,
  drinks: drinkReducer
});

/**
 * Delegates to the appReducer.
 *
 * @method
 * @private
 *
 * @param  {Object} state  The state object
 * @param  {Object} action The action
 * @return {Object}
 */
const rootReducer = (state:?Object, action:Object):Object => appReducer(state, action);

export default rootReducer;
