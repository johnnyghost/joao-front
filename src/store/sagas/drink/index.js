import { fork } from 'redux-saga/effects';
import { fetchDrink } from './watchGetDrinks';

/**
 * Github saga root.
 */
export function* drink():void {
  yield fork(fetchDrink);
}
