import { put, call } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { apiService } from 'core/services'
import DRINKS_CONSTANTS from 'store/constants/drink';

/**
 * Fetch the catalogue.
 */
function *getDrinks():void {
  try {
    const drinks = yield call(apiService.getDrinks);
    yield put({ type: DRINKS_CONSTANTS.GET_DRINK_SUCCESS, payload: drinks });

  } catch (error) {
    yield put({ type: DRINKS_CONSTANTS.ERROR });
  }
}

/**
 * Watch fetch catalogue saga.
 */
export function* fetchDrink():void {
  yield* takeLatest(DRINKS_CONSTANTS.FETCH_DRINK, getDrinks);
}
