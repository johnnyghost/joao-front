import { fork } from 'redux-saga/effects';
import { fetchFood } from './watchGetFoods';

/**
 * Github saga root.
 */
export function* food():void {
  yield fork(fetchFood);
}
