import { put, call } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { apiService } from 'core/services'
import FOOD_CONSTANTS from 'store/constants/food';

/**
 * Fetch the catalogue.
 */
function *getFoods():void {
  try {
    const foods = yield call(apiService.getFoods);
    yield put({ type: FOOD_CONSTANTS.GET_FOOD_SUCCESS, payload: foods });
  } catch (error) {
    console.log(error);
    yield put({ type: FOOD_CONSTANTS.ERROR });
  }
}

/**
 * Watch fetch catalogue saga.
 */
export function* fetchFood():void {
  yield* takeLatest(FOOD_CONSTANTS.FETCH_FOOD, getFoods);
}
