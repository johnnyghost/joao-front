import { fork } from 'redux-saga/effects';
import { food } from './food';
import { drink } from './drink';

/**
 * Saga root.
 */
export default function* root():void {
  yield fork(food);
  yield fork(drink);
}
