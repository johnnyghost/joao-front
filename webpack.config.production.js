var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var config = require('./webpack.config.common.js');

<<<<<<< HEAD
Object.assign(config, {

  devtool: 'source-map',

  plugins: config.plugins.concat([
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      output: {
        comments: false
      },
      compress: {
        warnings: false,
        screw_ie8: true
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new HtmlWebpackPlugin({
      isProduction: true,
      template: './template/index.html'
    })
  ])
})
=======
config.devtool = 'source-map';

config.plugins = config.plugins.concat([
  new webpack.optimize.OccurenceOrderPlugin(true),
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.CommonsChunkPlugin('app/scripts/vendors', '[name].[chunkhash].js'),
  new webpack.optimize.UglifyJsPlugin({
    output: {
      comments: false
    },
    compress: {
      warnings: false,
      screw_ie8: true
    }
  }),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  }),
  new HtmlWebpackPlugin({
    isProduction: true,
    template: './template/index.html'
  })
]);
>>>>>>> 27fee8253911288146a69ce7b7504a0e9e958fa4

module.exports = config;
